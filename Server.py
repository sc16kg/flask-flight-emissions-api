import requests, json, time, statistics
from flask import Flask, render_template
from flask_restful import Resource, Api, abort

app = Flask(__name__)
api = Api(app)

############### FLIGHTS ###############
country = "UK"
currency = "GBP"
locale = "en-GB"

# Having the airports saved in this format is necessary for skyscanner, the API endpoint it has for
# getting airports was not suitable
airports = {
    'LONDON': "LHR-sky",
    'MANCHESTER':"MAN-sky",
    'EDINBURGH':"EDI-sky",
    'BIRMINGHAM':"BHX-sky",
    'GLASGOW':"GLA-sky",
    'BRISTOL':"BRS-sky",
    'NEWCASTLE':"NCL-sky",
    'LIVERPOOL':"LPL-sky",
    'ABERDEEN':"ABZ-sky",
    'LEEDS':"LBA-sky",
    'SOUTHAMPTON':"SOU-sky",
    'CARDIFF':"CWL-sky",
    'DONCASTER':"DSA-sky",
    'EXETER':"EXT-sky",
    'BOURNEMOUTH':"BOH-sky",
    'NORWICH':"NWI-sky"
}
flightURL = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browseroutes/v1.0/UK/GBP/en-GB/%s/%s/%s"
flightHeaders = {
    "x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
	"x-rapidapi-key": "d85f589a12msh935996e3032e1d2p189a5cjsn829f12dfa8be"}
routeMatrixURL = "http://open.mapquestapi.com/directions/v2/routematrix?key=CyZVtMMn1EKVWzAy5ivAirFnG4axfpzN"

# Error handling for user putting invalid location in URL
def abort_if_location_doesnt_exist(airport_id):
    if airport_id not in airports:
        abort(404, message="Airport at location: {} doesn't exist".format(airport_id))

# Run tests on the web services
class TimedTests(Resource):
    def get(self):
        print("--- WEB SERVICE 1 TIME MEASUREMENTS ---")
        date = "2019/12/10"
        url = flightURL % ("LHR-sky", "MAN-sky", date)
        queryString = {"inboundpartialdate": date}
        i = 1
        times = []
        while i < 6:
            start = time.time()
            response=requests.request("GET", url, headers=flightHeaders,params=queryString)
            end = time.time()
            times.append(end-start)
            print("- TIME "+str(i)+":" + str(end-start))
            i = i + 1
        print("- MEAN RESPONSE TIME "+str(statistics.mean(times)))
        print("- STANDARD DEVIATION "+str(statistics.stdev(times)))
        print('\n')
        print("--- WEB SERVICE 2 TIME MEASUREMENTS ---")
        url = routeMatrixURL + "&from=london&to=manchester"
        i = 1
        times = []
        while i < 6:
            start = time.time()
            response = requests.request("POST", url)
            end = time.time()
            times.append(end-start)
            print("- TIME "+str(i)+":" + str(end-start))
            i = i + 1
        print("- MEAN RESPONSE TIME "+str(statistics.mean(times)))
        print("- STANDARD DEVIATION "+str(statistics.stdev(times)))
        print('\n')
        print("--- WEB SERVICE 3 TIME MEASUREMENTS ---")
        Em = Emissions()
        i = 1
        times = []
        while i < 6:
            start = time.time()
            emissions = Em.get(10000)
            end = time.time()
            times.append(end-start)
            print("- TIME "+str(i)+":" + str(end-start))
            i = i + 1
        print("- MEAN RESPONSE TIME "+str(statistics.mean(times)))
        print("- STANDARD DEVIATION "+str(statistics.stdev(times)))

# Flight data object used to store all the data for a particular journey
class FlightData(Resource):
    def __init__(self, cheapest, places, carrier, date, time, found):
        self.cheapest = cheapest
        self.places = places
        self.carrier = carrier
        self.date = date
        self.time = time
        self.found = found

    # Searches through the quotes and picks out the cheapest flight on the day specified
    # If it can't find flight on that date it returns the first flight in the JSON
    def setCheapest(self, data, date):
        cheapestQuote = 0
        price = 2000
        j = 0
        for i in data["Quotes"]:
            if((int(i["MinPrice"]) < price) and (i["OutboundLeg"]["DepartureDate"].split('T')[0]) == date):
                cheapestQuote = j
            j = j + 1
        self.cheapest = data["Quotes"][cheapestQuote]

    # gets the date and time of the cheapest flight from the quote data
    def setDateTime(self,cheapest):
        dateTime = cheapest["OutboundLeg"]["DepartureDate"]
        time = dateTime.split('T')[1]
        date = dateTime.split('T')[0]
        self.time = time
        self.date = date

    # gets the carrier ID from the cheapest quote and finds it's name in the carrier list
    def setCarrier(self, data):
        id = self.cheapest["OutboundLeg"]["CarrierIds"][0]
        for i in data["Carriers"]:
            if(i["CarrierId"] == id):
                name = i["Name"]
        self.carrier = name

    # if a flight is not found on the specified date this returns false
    def setFound(self,date):
        if(self.date != date):
            self.found = False

    # prints the flight object to command line
    def quickPrint(self):
        print(self.cheapest)
        print(self.carrier)
        print(self.date)
        print(self.time)

# Distance API calling resource
class Distance(Resource):
    def get(self, start, end):
        url = routeMatrixURL + "&from=" + start +"&to=" + end
        response = requests.request("POST", url)
        data = response.json()
        dist = data["distance"][1]
        return dist

# Sample distance returns an example output from the distance API
class SampleDistance(Resource):
    def get(self):
        data = json.loads('{ "allToAll": true, "distance": [ [ 0, 13.052, 29.369 ], [ 11.67, 0, 17.06 ], [ 28.441, 17.783, 0 ] ], "time": [ [ 0, 1037, 2150 ], [ 1068, 0, 1253 ], [ 2119, 1242, 0 ] ], "locations": [ { "latLng": { "lng": -104.984853, "lat": 39.738453 }, "adminArea4": "Denver County", "adminArea5Type": "City", "adminArea4Type": "County", "adminArea5": "Denver", "street": "", "adminArea1": "US", "adminArea3": "CO", "type": "s", "displayLatLng": { "lng": -104.984853, "lat": 39.738453 }, "linkId": 282041090, "postalCode": "", "sideOfStreet": "N", "dragPoint": false, "adminArea1Type": "Country", "geocodeQuality": "CITY", "geocodeQualityCode": "A5XAX", "adminArea3Type": "State" }, { "latLng": { "lng": -105.050335, "lat": 39.863462 }, "adminArea4": "City and County of Broomfield", "adminArea5Type": "City", "adminArea4Type": "County", "adminArea5": "Westminster", "street": "", "adminArea1": "US", "adminArea3": "CO", "type": "s", "displayLatLng": { "lng": -105.050335, "lat": 39.863462 }, "linkId": 282040216, "postalCode": "", "sideOfStreet": "N", "dragPoint": false, "adminArea1Type": "Country", "geocodeQuality": "CITY", "geocodeQualityCode": "A5XAX", "adminArea3Type": "State" }, { "latLng": { "lng": -105.27927, "lat": 40.015831 }, "adminArea4": "Boulder County", "adminArea5Type": "City", "adminArea4Type": "County", "adminArea5": "Boulder", "street": "", "adminArea1": "US", "adminArea3": "CO", "type": "s", "displayLatLng": { "lng": -105.27927, "lat": 40.015831 }, "linkId": 282039983, "postalCode": "", "sideOfStreet": "N", "dragPoint": false, "adminArea1Type": "Country", "geocodeQuality": "CITY", "geocodeQualityCode": "A5XAX", "adminArea3Type": "State" } ], "manyToOne": false, "info": { "copyright": { "text": "© 2018 MapQuest, Inc.", "imageUrl": "http://api.mqcdn.com/res/mqlogo.gif", "imageAltText": "© 2018 MapQuest, Inc." }, "statuscode": 0, "messages": [] } }')
        return data

# Emissions takes a distance input and calculates emissions related data for it
# Returns a dictionary
class Emissions(Resource):
    def get(self, distance):
        distance = float(distance)
        # 247 grams of greenhouse gasses per mile of a domestic flight
        emissions = round(distance * 0.247,2)
        # 66 grams of greenhouse gasses per mile of rail travel
        trainEmissions = round(emissions/0.066)
        # 43.4 grams of greenhouse gasses per mile of coach travel
        coachEmissions = round(emissions/0.0434)
        # young trees absorb just under 6kg of carbon dioxide a year
        trees = round(emissions/5.9)

        emissionsDict = {
            "Plane" : emissions,
            "Train" : trainEmissions,
            "Coach" : coachEmissions,
            "Trees" : trees
        }

        return emissionsDict

# Takes the name of the city the airport is in and returns its Skyscanner IATA style code
class Airport(Resource):
    def get(self, airport_id):
        airport_id = airport_id.upper()
        abort_if_location_doesnt_exist(airport_id)
        return airports[airport_id]

# Returns all of the available airports
class AirportList(Resource):
    def get(self):
        return airports

# Sample quote returns an example output from the Skyscanner API
class SampleQuote(Resource):
    def get(self):
        data = json.loads('{"Quotes":[{"QuoteId":1,"MinPrice":336.0,"Direct":true,"OutboundLeg":{"CarrierIds":[1864],"OriginId":81727,"DestinationId":60987,"DepartureDate":"2018-04-01T00:00:00"},"InboundLeg":{"CarrierIds":[851],"OriginId":60987,"DestinationId":81727,"DepartureDate":"2018-05-01T00:00:00"},"QuoteDateTime":"2018-03-08T04:54:00"}],"Places":[{"PlaceId":60987,"IataCode":"JFK","Name":"New York John F. Kennedy","Type":"Station","SkyscannerCode":"JFK","CityName":"New York","CityId":"NYCA","CountryName":"United States"},{"PlaceId":81727,"IataCode":"SFO","Name":"San Francisco International","Type":"Station","SkyscannerCode":"SFO","CityName":"San Francisco","CityId":"SFOA","CountryName":"United States"}],"Carriers":[{"CarrierId":851,"Name":"Alaska Airlines"},{"CarrierId":870,"Name":"jetBlue"},{"CarrierId":1721,"Name":"Sun Country Airlines"},{"CarrierId":1864,"Name":"Virgin America"}],"Currencies":[{"Code":"USD","Symbol":"$","ThousandsSeparator":",","DecimalSeparator":".","SymbolOnLeft":true,"SpaceBetweenAmountAndSymbol":false,"RoundingCoefficient":0,"DecimalDigits":2}]}')
        
        return data

# Flight calls the skyscanner API and formats the data into a dictionary that only contains the useful data
class Flight(Resource):
    def get(self, flyingFrom, flyingTo, date):
        flyingFrom = flyingFrom.upper()
        abort_if_location_doesnt_exist(flyingFrom)
        flyingFrom = airports[flyingFrom]

        flyingTo = flyingTo.upper()
        abort_if_location_doesnt_exist(flyingTo)
        flyingTo = airports[flyingTo]

        url = flightURL % (flyingFrom, flyingTo, date)
        queryString = {"inboundpartialdate": date}
        response=requests.request("GET", url, headers=flightHeaders,params=queryString)
        data = response.json()
        flight = FlightData({},{},"","","",True)
        flight.places = data["Places"]
        flight.setCheapest(data,date)
        flight.setDateTime(flight.cheapest)
        flight.setCarrier(data)

        data = {
            "Cheapest": flight.cheapest,
            "Places":flight.places,
            "Date": flight.date,
            "Time": flight.time,
            "Carrier": flight.carrier,
            "Found": flight.found
        }

        return data

# FlightWithEmissions calls all 3 web services in one go
class FlightWithEmissions(Resource):
    def get(self,flyingFrom, flyingTo, date):
        flyingFrom = flyingFrom.upper()
        abort_if_location_doesnt_exist(flyingFrom)
        flyingFrom = airports[flyingFrom]

        flyingTo = flyingTo.upper()
        abort_if_location_doesnt_exist(flyingTo)
        flyingTo = airports[flyingTo]

        url = flightURL % (flyingFrom, flyingTo, date)
        queryString = {"inboundpartialdate": date}
        response=requests.request("GET", url, headers=flightHeaders,params=queryString)
        data = response.json()
        flight = FlightData({},{},"","","",True)
        flight.places = data["Places"]
        flight.setCheapest(data,date)
        flight.setDateTime(flight.cheapest)
        flight.setCarrier(data)

        distStart = data["Places"][0]["CityName"]
        distEnd = data["Places"][1]["CityName"]
        Dist = Distance()
        dist = Dist.get(distStart,distEnd)
        Em = Emissions()
        emissions = Em.get(dist)

        data = {
            "Cheapest": flight.cheapest,
            "Places":flight.places,
            "Date": flight.date,
            "Time": flight.time,
            "Carrier": flight.carrier,
            "Found": flight.found,
            "Distance": dist,
            "Emissions": emissions
        }

        return data

api.add_resource(TimedTests,'/tests/')
api.add_resource(Emissions,'/emissions/<string:distance>/')
api.add_resource(Distance,'/distance/<string:start>/<string:end>/')
api.add_resource(Airport,'/airport/<string:airport_id>/')
api.add_resource(AirportList,'/airportlist','/airportlist/')
api.add_resource(SampleQuote,'/quote/sample/','/quotes/sample')
api.add_resource(SampleDistance,'/distance/sample/','/distance/sample')
api.add_resource(Flight,'/flight/<string:flyingFrom>/<string:flyingTo>/<string:date>/')
api.add_resource(FlightWithEmissions,'/flight/emissions/<string:flyingFrom>/<string:flyingTo>/<string:date>/')
if __name__ == '__main__':
    app.run(debug=True)
