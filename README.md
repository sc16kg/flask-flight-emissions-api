# COMP3211 - Distributed Systems - Coursework 2

## Screenshots

![home](Screenshots/home.png)
![search](Screenshots/search.png)
![results](Screenshots/results.png)
![emissions](Screenshots/emissions.png)


### Installing

Download the repository and unpack it

run ./setup.sh

This will create a new virtual environment called demo, install all the prerequisites in demo, and launch the servers.
This script works on DEC10 machines where the terminal is 'gnome-terminal', to run it elsewhere on other linux distros it may be necessary to run the servers manually in seperate terminal windows using the commands:


```
python Server.py
```
```
python Client.py
```
The web application can then be accessed at:


```
localhost:5001/
```

## Authors

* **Kieran James Gray** - *SC16KG*
