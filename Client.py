import requests, json, hashlib, os, time
from flask import Flask, render_template, redirect, flash
from flask_restful import Resource, Api, abort, reqparse
from flask_wtf import FlaskForm
from wtforms import IntegerField, SelectField, StringField, BooleanField
from wtforms.validators import InputRequired, Length, Regexp

################################# APP SETUP #################################

app = Flask(__name__)
WTF_CSRF_ENABLED = True
SECRET_KEY = hashlib.sha256('Super Secret Key one two three'.encode('utf-8')).hexdigest()
app.secret_key = os.urandom(24)

################################# FORMS #################################

class FlightSearchForm(FlaskForm):
    departing = SelectField('Departing',choices=[('LONDON','LONDON'),('MANCHESTER','MANCHESTER'),
        ('EDINBURGH','EDINBURGH'),('GLASGOW','GLASGOW'),
        ('BRISTOL','BRISTOL'),('NEWCASTLE','NEWCASTLE'),
        ('LIVERPOOL','LIVERPOOL'),('ABERDEEN','ABERDEEN'),        
        ('LEEDS','LEEDS'),('SOUTHAMPTON','SOUTHAMPTON'),        
        ('CARDIFF','CARDIFF'),('DONCASTER','DONCASTER'),        
        ('EXETER','EXETER'),('BOURNEMOUTH','BOURNEMOUTH'),
        ('NORWICH','NORWICH')], default='LONDON')
    arriving = SelectField('Arriving',choices=[('LONDON','LONDON'),('MANCHESTER','MANCHESTER'),
        ('EDINBURGH','EDINBURGH'),('GLASGOW','GLASGOW'),
        ('BRISTOL','BRISTOL'),('NEWCASTLE','NEWCASTLE'),
        ('LIVERPOOL','LIVERPOOL'),('ABERDEEN','ABERDEEN'),        
        ('LEEDS','LEEDS'),('SOUTHAMPTON','SOUTHAMPTON'),        
        ('CARDIFF','CARDIFF'),('DONCASTER','DONCASTER'),        
        ('EXETER','EXETER'),('BOURNEMOUTH','BOURNEMOUTH'),
        ('NORWICH','NORWICH')], default='MANCHESTER')
    outboundDate = StringField('Depature Date', validators=[InputRequired(),Regexp('([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))',message="Not A Valid Date")])
    inboundDate = StringField('Return Date')

class EmissionsForm(FlaskForm):
    distance = IntegerField('Distance',validators=[InputRequired()])

################################# FUNCTIONS #################################
#
#   Description: calls the server API to get flight data
#   Parameters: String departing - Airport flying from, arriving - Airport flying to, date - date flying
#   Return: Flight data
#

def getFlights(departing,arriving,date):
    url = "http://localhost:5000/flight/" + departing + "/" + arriving + "/" + date
    try:
        response=requests.request("GET", url)
        data=response.json()
        return data
    except json.decoder.JSONDecodeError:
        return ({"failed":400})
#
#   Description: find the distance between two airports
#   Parameters: JSON data - flight data from the skyscanner api to find the locations of the airports
#   Return: String - distance
#

def getDistance(data):
    distStart = data[0]["CityName"]
    distEnd = data[1]["CityName"]
    distURL = "http://localhost:5000/distance/"+distStart+"/"+distEnd
    try:
        response = requests.request("GET", distURL)
        data = response.json()
        return data
    except json.decoder.JSONDecodeError:
        return({"failed":400})

#
#   Description: calls server API to get flight emission data
#   Parameters: String distance - distance between two airports
#   Return: Dict - Plane emissions in kg, miles of travel for train and coach to make the same emissions, trees required
#

def getEmissions(distance):
    url = "http://localhost:5000/emissions/" + str(distance)
    try:
        response = requests.request("GET", url)
        emissions = response.json()
        return emissions
    except json.decoder.JSONDecodeError:
        return({"failed":400})

#
#   Description: calls server API to get Flight data, distance, and emission data in one
#   parameters: String departing - Airport flying from, arriving - Airport flying to, date - date flying
#   Return: Dict - Cheapest flight data, distance in miles, plane emissions in kg, 
#           miles of travel for train and coach to make the same emissions, trees required
#

def getFlightsAndEmissions(departing,arriving,date):
    url = "http://localhost:5000/flight/emissions/" + departing + "/" + arriving + "/" + date
    try:
        response=requests.request("GET", url)
        data=response.json()
        return data
    except:
        return({"failed":400})

################################# APP ROUTES ############################################

####### Home Page #######

@app.route('/')
def home():
    return render_template('home.html',
        title="Home")

####### Page for Failed API Call #######

@app.route('/failed/<string:api>', methods=['GET','POST'])
def failed(api):
    flash(api+" API RETURNED AN ERROR")
    form = FlightSearchForm()
    if form.validate_on_submit():
        if(form.inboundDate.data is None):
            url= '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data
        url = '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data +'/'+form.inboundDate.data
        return redirect(url)
    return render_template('flightsearch.html', title="Flight Search", form=form)

####### Airport List #######

@app.route('/airportlist/')
def airportList():
    start = time.time()
    response = requests.request("GET","http://localhost:5000/airportlist/")
    end = time.time()
    aTime = round((end-start),5)
    data = response.json()
    return render_template('airports.html',title="Airports",airports=data,time=aTime)

####### Flight Search #######

@app.route('/flightsearch/', methods=['GET','POST'])
def flightSearch():
    form = FlightSearchForm()
    if form.validate_on_submit():
        if(form.inboundDate.data is None):
            url= '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data
        url = '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data +'/'+form.inboundDate.data
        return redirect(url)
    return render_template('flightsearch.html',
    title="Flight Search",
    form=form)

####### Emissions Calculator #######

@app.route('/calc/', methods=['GET','POST'])
def emissionsCalculator():
    form = EmissionsForm()
    emissions = 0
    trees = 0
    eTime=0
    if form.validate_on_submit():
        start=time.time()
        data = getEmissions(form.distance.data)
        end=time.time()
        eTime=round(end-start,5)
        emissions = data["Plane"]
        trees = data["Trees"]
        return render_template("emissions.html", title="Emissions", form=form, emissions=emissions, trees=trees, eTime=eTime)
    return render_template("emissions.html", title="Emissions Calculator", form=form, emissions=emissions, trees=trees, eTime=eTime)

####### Flight data for a return flight #######

@app.route('/flight/<string:departing>/<string:arriving>/<string:outboundDate>/<string:inboundDate>', methods=['GET','POST'])
def returnFlight(departing,arriving,outboundDate,inboundDate):
    # Form allows user to search again from flight result page
    form=FlightSearchForm()
    if form.validate_on_submit():
        url = '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data +'/'+form.inboundDate.data
        return redirect(url)

    oFlight = getFlights(departing,arriving,outboundDate)
    if "failed" in oFlight:
        return redirect('/failed/SKYSCANNER')

    start = time.time()
    iFlight = getFlights(arriving,departing,inboundDate)
    end = time.time()
    fTime = round((end-start),5)
    if "failed" in iFlight:
        return redirect('/failed/SKYSCANNER')

    start = time.time()
    distance = getDistance(oFlight["Places"])
    end = time.time()
    dTime = round((end-start),5)

    start = time.time()
    emissions = getEmissions(distance)
    end = time.time()
    eTime = round((end-start),5)
    if "failed" in emissions:
        return redirect('/failed/EMISSIONS')
    distance = float(distance)
    return render_template('flight.html', title="flight",form=form, fTime=fTime,dTime=dTime,eTime=eTime,oPrice=oFlight["Cheapest"]["MinPrice"], oTime=oFlight["Time"], departing=departing,arriving=arriving,oDate=oFlight["Date"],cOQ=oFlight["Cheapest"],cOC=oFlight["Carrier"],iPrice=iFlight["Cheapest"]["MinPrice"], iTime=iFlight["Time"],iDate=iFlight["Date"],cIQ=iFlight["Cheapest"],cIC=iFlight["Carrier"],distance=round(distance),emissions=emissions["Plane"],trainEmissions=emissions["Train"],coachEmissions=emissions["Coach"],outQuoteFound=oFlight["Found"], inQuoteFound=iFlight["Found"], trees=emissions["Trees"])

####### Flight data for a one way flight #######

@app.route('/flight/<string:departing>/<string:arriving>/<string:outboundDate>/', methods=['GET','POST'])
def oneWayFlight(departing,arriving,outboundDate):
    form=FlightSearchForm()
    if form.validate_on_submit():
        url = '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data +'/'+form.inboundDate.data
        return redirect(url)

    start = time.time()
    flight = getFlights(departing,arriving,outboundDate)
    end = time.time()
    fTime = round((end-start),5)
    if "failed" in flight:
        return redirect('/failed/SKYSCANNER')

    start = time.time()
    distance = getDistance(flight["Places"])
    end = time.time()
    dTime = round((end-start),5)

    start = time.time()
    emissions = getEmissions(distance)
    end = time.time()
    eTime = round((end-start),5)
    return render_template('flightOneWay.html', title="flight", form=form,fTime=fTime,dTime=dTime,eTime=eTime, price=flight["Cheapest"]["MinPrice"], time=flight["Time"], departing=departing,arriving=arriving,date=flight["Date"],cheapestQuote=flight["Cheapest"],carrier=flight["Carrier"],quoteFound=flight["Found"],distance=round(distance),emissions=emissions["Plane"],trainEmissions=emissions["Train"],coachEmissions=emissions["Coach"], trees=emissions["Trees"])

####### Flight and emissions data for a return flight #######

@app.route('/flight/emissions/<string:departing>/<string:arriving>/<string:outboundDate>/<string:inboundDate>', methods=['GET','POST'])
def flightWithEmissions(departing,arriving,outboundDate, inboundDate):
    form=FlightSearchForm()
    if form.validate_on_submit():
        url = '/flight/'+form.departing.data+'/'+form.arriving.data+'/'+form.outboundDate.data +'/'+form.inboundDate.data
        return redirect(url)
    oFlight = getFlightsAndEmissions(departing,arriving,outboundDate)
    if "failed" in oFlight:
        return redirect('/failed/SKYSCANNER')
    iFlight = getFlightsAndEmissions(arriving,departing,inboundDate)
    if "failed" in iFlight:
        return redirect('/failed/SKYSCANNER')
    return render_template('flight.html', title="flight",form=form, fTime=0,dTime=0,eTime=0,oPrice=oFlight["Cheapest"]["MinPrice"], oTime=oFlight["Time"], departing=departing,arriving=arriving,oDate=oFlight["Date"],cOQ=oFlight["Cheapest"],cOC=oFlight["Carrier"],iPrice=iFlight["Cheapest"]["MinPrice"], iTime=iFlight["Time"],iDate=iFlight["Date"],cIQ=iFlight["Cheapest"],cIC=iFlight["Carrier"],distance=round(iFlight["Distance"]),emissions=iFlight["Emissions"]["Plane"],trainEmissions=iFlight["Emissions"]["Train"],coachEmissions=iFlight["Emissions"]["Coach"],outQuoteFound=oFlight["Found"], inQuoteFound=iFlight["Found"], trees=iFlight["Emissions"]["Trees"])

# Run the client on port 5001 because the server is running on port 5000
if __name__ == '__main__':
    app.run(debug=True, port=5001)
